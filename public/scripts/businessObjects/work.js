import bank from './bank.js';

const work = (function() {
    let pay = 0;
    const payFromWorkPeriod = 100;

    return {
        getPay: function() {
            return pay;
        },
        work: function() {
            pay += payFromWorkPeriod;
        },
        transferToBank: function() {
            if (bank.hasLoan()) {
                const amountToLoan = pay * 0.1;
                const amountToBank = pay - amountToLoan;
                bank.payBackLoan(amountToLoan);
                bank.deposit(amountToBank);
                pay = 0;
            } else {
                bank.deposit(pay);
                pay = 0;
            }
        },
        payLoan: function() {
            if (bank.getLoanAmount() >= pay) {
                bank.payBackLoan(pay);
                pay = 0;
            } else {
                const amountToLoan = bank.getLoanAmount();
                bank.payBackLoan(amountToLoan);
                pay -= amountToLoan;
            }
        }
    };
})();

export default work;