import bank from './bank.js';

const store = (function() {
    return {
        buy: function(totalPrice) {
            if (totalPrice > bank.getBalance()) {
                throw 'You do not not have enough money in your account to buy this computer.';
            }
            
            bank.withdraw(totalPrice);
        },
        calculateTotalPrice: function(laptop, software) {
            let totalPrice = laptop.price;
            Object.values(software).forEach(application => {
                if (application.checked) {
                    totalPrice += application.price;
                }
            })
            return totalPrice;
        }
    };
})();

export default store;