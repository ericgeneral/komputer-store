const bank = (function() {
    let balance = 200;
    let loan = 0;

    return {
        getBalance: function() {
            return balance;
        },
        deposit: function(amount) {
            balance += amount;
        },
        withdraw: function(amount) {
            if (amount > balance) {
                throw `You cannot withdraw more than your current balance of ${balance}.`;
            }
            balance -= amount;
        },
        getLoanAmount: function() {
            return loan;
        },
        hasLoan: function() {
            return loan > 0;
        },
        addLoan: function(newLoan) {
            if (this.hasLoan()) {
                throw 'You already have a loan.';
            }
            if (newLoan > 2 * balance) {
                throw `You cannot get a loan greater than ${2 * balance}.`;
            }
            loan = newLoan;
            this.deposit(newLoan); 
        },
        payBackLoan(amount) {
            if (!this.hasLoan()) {
                throw 'You do not have a loan.';
            }
            if (amount > loan) {
                throw 'You cannot pay back more than you owe.';
            }
            loan -= amount;
        }
    };
})();

export default bank;
