import bank from './businessObjects/bank.js';
import store from './businessObjects/store.js';
import work from './businessObjects/work.js';

const mainGridElement = document.getElementById('mainGrid');
/* Laptop Section */
const laptopSelectElement = document.getElementById('laptopSelect');
const featuresElement = document.getElementById('features');
/* Software Section */
const softwareElement = document.getElementById('software');
/* Details Section */
const laptopNameElement = document.getElementById('laptopName');
const laptopDescriptionElement = document.getElementById('laptopDescription');
const laptopPriceElement = document.getElementById('laptopPrice');
const laptopImageElement = document.getElementById('laptopImage');
const buyNowButtonElement = document.getElementById('buyNowButton');
const addedSoftwareElement = document.getElementById('addedSoftware');
/* Bank Section */
const balanceAmountElement = document.getElementById('balanceAmount');
const loanElement = document.getElementById('loan');
const loanAmountElement = document.getElementById('loanAmount');
const getLoanButtonElement = document.getElementById('getLoanButton');
/* Get Loan Modal Window */
const getLoanModalElement = document.getElementById('getLoanModal');
const okLoanModalButtonElement = document.getElementById('okLoanModalButton');
const cancelLoanModalButtonElement = document.getElementById('cancelLoanModalButton');
const addLoanErrorMessageElement = document.getElementById('addLoanErrorMessage');
const getLoanAmountInputElement = document.getElementById('getLoanAmountInput');
/* Work Section */
const payAmountElement = document.getElementById('payAmount');
const workButtonElement = document.getElementById('workButton');
const bankButtonElement = document.getElementById('bankButton');
const payLoanButtonElement = document.getElementById('payLoanButton');
/* Purchase Modal Window */
const purchaseModalElement = document.getElementById('purchaseModal');
const purchasedComputerElement = document.getElementById('purchasedComputer');
const purchasedSoftwareElement = document.getElementById('purchasedSoftware');
const okPurchaseModalButtonElement = document.getElementById('okPurchaseModalButton');
const amountWithdrawnElement = document.getElementById('amountWithdrawn');
/* Error Modal Window */
const errorModalElement = document.getElementById('errorModal');
const errorMessage = document.getElementById('errorMessage');
const okErrorModalButton = document.getElementById('okErrorModalButton');

const computersBaseUrl = 'https://noroff-komputer-store-api.herokuapp.com/';
const softwareBaseUrl = 'https://softwarelist-511d0-default-rtdb.firebaseio.com/';
let laptops = [];
let software = [];
let selectedLaptop = null;

const handleOkErrorModalClick = () => {
    errorMessage.innerText = '';
    hideModal(errorModalElement);
}

fetch(computersBaseUrl + 'computers')
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToSelect(laptops));

fetch(softwareBaseUrl + 'apps.json')
    .then(response => response.json())
    .then(data => software = data)
    .then(software => addApplicationsToSoftwareSection(software));

/************************
* Laptops Section
************************/
const addLaptopsToSelect = (laptops) => {
    laptops.forEach(laptop => addLaptopToSelect(laptop));
    selectedLaptop = laptops[0];
    updateSpecs();
    updateDetailsSection();
}

const addLaptopToSelect = (laptop) => {
    const option = document.createElement('option');
    option.value = laptop.id;
    option.appendChild(document.createTextNode(laptop.title));
    laptopSelectElement.appendChild(option);
}

const handleLaptopChange = (event) => {
    selectedLaptop = laptops[event.target.selectedIndex];
    updateSpecs();
    updateDetailsSection();
}

const updateSpecs = () => {
    featuresElement.textContent = '';
    selectedLaptop.specs.forEach(spec => {
        const listElement = document.createElement('li');
        listElement.appendChild(document.createTextNode(spec));
        featuresElement.appendChild(listElement);
    });
}

const resetSelectedComputer = () => {
    laptopSelectElement.selectedIndex = 0;
    selectedLaptop = laptops[0];
    updateSpecs();
    updateDetailsSection();
}

/************************
* Software Section
************************/
const addApplicationsToSoftwareSection = (applications) => {
    Object.values(applications).forEach(application => addApplicationToSoftwareSection(application));
}

const addApplicationToSoftwareSection = (application) => {
    const div = document.createElement('div');
    div.className = 'amountGrid';
    div.appendChild(createSoftwareCheckbox(application));
    div.appendChild(createPriceForSoftwareCheckbox(application));
    softwareElement.appendChild(div);
}

const createSoftwareCheckbox = (application) => {
    const leftDiv = document.createElement('div');
    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.id = application.id;
    checkbox.name = 'software';
    leftDiv.appendChild(checkbox);
    leftDiv.appendChild(createLabelForSoftwareCheckbox(application));
    return leftDiv;
}

const createLabelForSoftwareCheckbox = (application) => {
    const label = document.createElement('label');
    label.setAttribute('for', application.id);
    label.appendChild(document.createTextNode(application.title));
    return label;
}

const createPriceForSoftwareCheckbox = (application) => {
    const applicationPrice = document.createElement('div');
    applicationPrice.className = 'amountItem';
    applicationPrice.appendChild(document.createTextNode(formatAmount(application.price)));
    return applicationPrice;
}

const handleSoftwareClick = (event) => {
    if (event.target.type === 'checkbox') {
        if (event.target.checked) {
            software[event.target.id].checked = true;
        } else {
            software[event.target.id].checked = false;
        }
    }
    updateDetailsSection();
}

const resetSelectedSoftware = () => {
    softwareElement.querySelectorAll("div > input").forEach(node => {
        node.checked = false;
    })
    updateDetailsSection();
}

/************************
* Bank Section
************************/
const updateBankSection = () => {
    balanceAmountElement.innerText = formatAmount(bank.getBalance());
    loanAmountElement.innerText = formatAmount(bank.getLoanAmount());
    if (bank.hasLoan()) {
        loanElement.style.visibility = 'visible';
    } else {
        loanElement.style.visibility = 'hidden';
    }
}

const handleGetLoanClick = () => {
    if (bank.hasLoan()) {
        errorMessage.innerText = 'You already have a loan.';
        showModal(errorModalElement);
        return;
    }
    addLoanErrorMessageElement.style.visibility = 'hidden';
    addLoanErrorMessageElement.innerText = '';
    showModal(getLoanModalElement);
    getLoanAmountInputElement.focus();
}

const getLoanFromInput = () => {
    const loanAmount = parseFloat(getLoanAmountInputElement.value);
    if (isNaN(loanAmount)) {
        throw `${getLoanAmountInputElement.value} is not a valid number.`;
    }
    if (loanAmount <= 0) {
        throw 'Your loan must be a positive number.';
    }
    return loanAmount;
}

const handleOkLoanModalClick = () => {
    try {
        const loanAmount = getLoanFromInput();
        bank.addLoan(loanAmount);
        updateBankSection();
        updateWorkSection();
        hideModal(getLoanModalElement);
    } catch (error) {
        getLoanAmountInputElement.value = '';
        addLoanErrorMessageElement.style.visibility = 'visible';
        addLoanErrorMessageElement.innerText = error;
        getLoanAmountInputElement.focus();
    }
}

const handleCancelLoanModalClick = () => {
    hideModal(getLoanModalElement);
}

/************************
* Work Section
************************/
const updateWorkSection = () => {
    if (bank.hasLoan()) {
        payLoanButtonElement.style.display = 'block';
    } else {
        payLoanButtonElement.style.display = 'none';
    }
    payAmountElement.innerText = formatAmount(work.getPay());
}

const handleWorkButtonClick = () => {
    work.work();
    updateWorkSection();
}

const handleBankButtonClick = () => {
    try {
        work.transferToBank();
        updateWorkSection();
        updateBankSection();
    } catch(error) {
        errorMessage.innerText = error;
        showModal(errorModalElement);
    }
}

const handlePayLoanButtonClick = () => {
    try {
        work.payLoan();
        updateWorkSection();
        updateBankSection();
    } catch(error) {
        errorMessage.innerText = error;
        showModal(errorModalElement);
    }
}

/************************
* Details Section
************************/
const updateDetailsSection = () => {
    laptopNameElement.innerText = selectedLaptop.title;
    laptopDescriptionElement.innerText = selectedLaptop.description;
    laptopPriceElement.innerText = formatAmount(store.calculateTotalPrice(selectedLaptop, software));
    laptopImageElement.src = computersBaseUrl + selectedLaptop.image;
    showSelectedApplicationsOnElement(addedSoftwareElement);
}

const showSelectedApplicationsOnElement = (softwareElement) => {
    softwareElement.innerText = '';
    Object.values(software).forEach(application => {
        addSelectedApplication(application, softwareElement);
    })
}

const addSelectedApplication = (application, softwareElement) => {
    if (application.checked) {
        const liElement = document.createElement('li');
        liElement.appendChild(document.createTextNode(application.title));
        softwareElement.appendChild(liElement);
    }
}

const handleBuyNowButtonClick = () => {
    try{
        const totalPrice = store.calculateTotalPrice(selectedLaptop, software);
        store.buy(totalPrice);
        updateBankSection();
        displayPurchase(totalPrice, software);
        resetSelectedComputer();
        resetSelectedSoftware();
    } catch (error) {
        errorMessage.innerText = error;
        showModal(errorModalElement);
    }
}

const displayPurchase = (totalPrice, software) => {
    purchasedComputerElement.innerText = laptopNameElement.innerText;
    amountWithdrawnElement.innerText = formatAmount(totalPrice) + ' was withdrawn from your account.';
    showSelectedApplicationsOnElement(purchasedSoftwareElement);
    showModal(purchaseModalElement);
}

const handleOkPurchaseModalButtonClick = () => {
    hideModal(purchaseModalElement);
}

/************************
* Event Listeners
************************/
document.addEventListener("DOMContentLoaded", () => {
    updateBankSection();
    updateWorkSection();
});

laptopSelectElement.addEventListener('change', handleLaptopChange);

softwareElement.addEventListener('click', handleSoftwareClick);

getLoanButtonElement.addEventListener('click', handleGetLoanClick);
okLoanModalButtonElement.addEventListener('click', handleOkLoanModalClick);
cancelLoanModalButtonElement.addEventListener('click', handleCancelLoanModalClick);

workButtonElement.addEventListener('click', handleWorkButtonClick);
bankButtonElement.addEventListener('click', handleBankButtonClick);
payLoanButtonElement.addEventListener('click', handlePayLoanButtonClick);

buyNowButtonElement.addEventListener('click', handleBuyNowButtonClick);

okPurchaseModalButtonElement.addEventListener('click', handleOkPurchaseModalButtonClick);

okErrorModalButton.addEventListener('click', handleOkErrorModalClick);


/************************
* Helpers
************************/
const showModal = (modal) => {
    modal.style.display = 'block';
    mainGridElement.style.opacity = '35%';
}

const hideModal = (modal) => {
    modal.style.display = 'none';
    mainGridElement.style.opacity = '100%';
}

const formatAmount = (amount) => {
    return amount + ' NOK';
}